
/**
 * Décrivez votre interface Phrase ici.
 *
 * @author  Xavier Blanchette-Noël
 * @version 1.0
 */

public interface Phrase
{
    /**
     * Retourne le 1er mot de l'enregistrement, une String nommée enr
     *
     * @param  enr String - l'enregistrement
     * @return String - 1er mot de l'enregistrement
     */
    String getPremierMot(String enr);
    
    /**
     * Retourne le 2e mot de l'enregistrement, une String nommée enr
     *
     * @param  enr String - l'enregistrement
     * @return 2e mot de l'enregistrement
     */
    String getPDeuxiemeMot(String enr);
}
