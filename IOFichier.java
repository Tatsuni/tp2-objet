/**
 * La classe IOFichier implémente différentes fonctions de lecture et
 * d'écriture de fichier.
 *
 * @author Xavier Blanchette-Noël
 * @version 1.0
 */
import java.io.*;
public class IOFichier implements IOinterface, Phrase
{
    private String nomFichier;

    /**
     * Constructeur d'objets de classe IOFichier
     */
    public IOFichier(String nomF)
    {
        this.nomFichier = nomF;
    }

    /**
     * affiche à l'écran tout le fichier
     *
     * @param
     * @return  void
     */
    public void afficheFichier()
    {
        try {
            String line = null;
            FileReader fileReader = new FileReader(this.nomFichier);
            
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }   
            bufferedReader.close();
        }
         
        catch (FileNotFoundException e) {
            System.out.println("Incapable d'ouvrir le fichier '" + this.nomFichier + "'");
        }
        
        catch (IOException e) {
            System.out.println(
            "Erreur lors de la lecture du fichier '" + this.nomFichier + '"');
        }
    }
    
   
    
    /**
     * retourne tout le fichier dans un tableau de String
     *
     * @param
     * @return String[] un tableau de String
     */
    public String[] retourneFichier()
    {   
        String[] tableau = new String[10];
        try {
            String line = "";
            int i = 0;
            FileReader fileReader = new FileReader(this.nomFichier);
            
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while ((line = bufferedReader.readLine()) != null && i < 10) {
                tableau[i] = line;
                i++;
            }
            bufferedReader.close();
        }
         
        catch (FileNotFoundException e) {
            System.out.println("Incapable d'ouvrir le fichier '" + this.nomFichier + "'");
        }
        
        catch (IOException e) {
            System.out.println(
            "Erreur lors de la lecture du fichier '" + this.nomFichier + ".");
        }
        
        return tableau;
    }

    /**
     * ajoute l'enregistrement à la fin du fichier
     *
     * @param  enr    l'enregistrement à ajouter au fichier
     * @return void       
     */
    public void ajouteEnregistrement(String enr)
    {
        
        try {
            FileWriter fileWriter = new FileWriter(this.nomFichier, true);
            String clientID = getClientId();
            fileWriter.write(clientID + " " + enr + "\n");
            fileWriter.close();
        }
            
        catch (IOException e) {
        System.out.println(
        "Erreur lors de l'ajout au fichier '" + this.nomFichier + "'");
        }
    }
    
	/**
     *  retourne l'id à ajouter sur le prochain enregistrement.
     *  
     *  @param
     *  @return String du numéro à mettre avant le nom du client.
     */
     public String getClientId()
    {
    	String clientID = new String();
        try {
            String lastLine = "";
            String line = null;
            FileReader fileReader = new FileReader(this.nomFichier);
            
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while ((line = bufferedReader.readLine()) != null) {
                lastLine = line;
            }   
            bufferedReader.close();
            
            if (lastLine == "") {
                clientID = "0";
            } else {
                int id = Integer.parseInt(getPremierMot(lastLine)) + 1;
                clientID = new StringBuilder().append(id).toString();
            }
        }
         
        catch (FileNotFoundException e) {
            System.out.println("Incapable d'ouvrir le fichier '" + this.nomFichier + "'");
        }
        
        catch (IOException e) {
            System.out.println(
            "Erreur lors de la lecture du fichier '" + this.nomFichier + '"');
        }
        
        return clientID;
    }    

    /**
     * retourne tout le nom à partir de son id
     *
     * @param le id du nom recherché
     * @return String le nom recherché
     */
    public String trouverNom(String id)
    {
        String nom = new String();
        String[] fullLine = new String[255];
        String line = "";
        
        try {
            FileReader fileReader = new FileReader(this.nomFichier);
            
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            while ((line = bufferedReader.readLine()) != null) {
                if (getPremierMot(line).equals(id)) {
                    nom = getPDeuxiemeMot(line);
                    break;
                }
            }   
            bufferedReader.close();
        }
         
        catch (FileNotFoundException e) {
            System.out.println("Incapable d'ouvrir le fichier '" + this.nomFichier + "'");
        }
        
        catch (IOException e) {
            System.out.println(
            "Erreur lors de la lecture du fichier '" + this.nomFichier + '"');
        }
        return nom;
    }
    
    /**
     * Retourne le 1er mot de l'enregistrement, une String nommée enr
     *
     * @param  enr String - l'enregistrement
     * @return String - 1er mot de l'enregistrement
     */
    public String getPremierMot(String enr)
    {
    	String[] splitArray = enr.split(" ");
    	
    	return splitArray[0];
    }
    
    /**
     * Retourne le 2e mot de l'enregistrement, une String nommée enr
     *
     * @param  enr String - l'enregistrement
     * @return 2e mot de l'enregistrement
     */
    public String getPDeuxiemeMot(String enr)
    {
    	String[] splitArray = new String[255];
    	splitArray = enr.split(" ");
    	
    	return splitArray[1];
    }
}