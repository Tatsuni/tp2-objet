

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ClientTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ClientTest
{
    String nomClient;
    /**
     * Default constructor for test class ClientTest
     */
    public ClientTest(String nomF)
    {
       this.nomClient = nomF;
    }
    
    public static void main(String[] args)
    {
        Client client1 = new Client("Xavier");
        Client client2 = new Client("Michael");
        Client client3 = new Client("Ezequiel");
        
        IOFichier fichier = new IOFichier("test.txt");
        
        fichier.ajouteEnregistrement(client1.clientName);
        fichier.ajouteEnregistrement(client2.clientName);
        fichier.ajouteEnregistrement(client3.clientName);
        
        //La valeur imprimée devrait être Xavier
        System.out.println(fichier.trouverNom("0"));
        //La valeur imprimée devrait être Ezequiel
        System.out.println(fichier.trouverNom("2"));
        
        String[] tableauFichier = fichier.retourneFichier();
        
        fichier.afficheFichier();
    }
}
