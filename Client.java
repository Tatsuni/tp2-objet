
/**
 * Classe pour l'objet client. Un client comporte un nom.
 *
 * @author Xavier Blanchette-Noël
 * @version 1.0
 */
public class Client extends IOFichier
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    public String clientName;

    /**
     * Constructeur d'objets de classe Client
     */
    public Client(String nomF)
    {
        // initialisation des variables d'instance
        super(nomF);
        this.clientName = nomF;
        
    }
}
