
/**
 * Classe pour l'objet produit. Comporte un nom de produit.
 *
 * @author Xavier Blanchette-Noël
 * @version 1.0
 */
public class Produit extends IOFichier
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    public String productName;

    /**
     * Constructeur d'objets de classe Produit
     */
    public Produit(String nomF)
    {
        // initialisation des variables d'instance
        super(nomF);
        this.productName = nomF;
    }


}
