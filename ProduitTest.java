

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ProduitTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ProduitTest
{
    String productName;
    /**
     * Default constructor for test class ProduitTest
     */
    public ProduitTest(String nomF)
    {
        this.productName = nomF;
    }

    public static void main(String[] args)
    {
        Produit produit1 = new Produit("Savon");
        Produit produit2 = new Produit("Tires");
        Produit produit3 = new Produit("Lolipop");
        
        IOFichier fichier = new IOFichier("test.txt");
        
        fichier.ajouteEnregistrement(produit1.productName);
        fichier.ajouteEnregistrement(produit2.productName);
        fichier.ajouteEnregistrement(produit3.productName);
        
        //La valeur imprimée devrait être Savon
        System.out.println(fichier.trouverNom("0"));
        //La valeur imprimée devrait être Lolipop
        System.out.println(fichier.trouverNom("2"));
        
        String[] tableauFichier = fichier.retourneFichier();
        
        fichier.afficheFichier();
    }
}
